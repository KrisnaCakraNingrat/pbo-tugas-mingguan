package kedua;

import java.util.Scanner;

public class PemilikKos extends AwalData{
    private String username;
    private String nama;
    private String alamat;
    private int nohp;
    private int harga;
    private int tgl;
    private int pil;

    Scanner input = new Scanner(System.in);

    @Override
    public void data() {
        System.out.println("----------------- Masukkan Data Diri Anda -----------------");//Behavior
        System.out.print("Masukkan Nama Anda            : ");//Behavior
        nama = input.nextLine();
        System.out.print("Masukkan Alamat Anda          : ");//Behavior
        alamat = input.nextLine();
        System.out.print("Masukkan No HP Anda           : ");//Behavior
        nohp = input.nextInt();
        System.out.print("Masukkan Tanggal Masuk        : ");//Behavior
        tgl = input.nextInt();
        System.out.print("Masukkan Pilihan Kamar Anda   : ");//Behavior
        pil = input.nextInt();
    }

    @Override
    public void harga() {
        switch (pil){
            case 1:
                harga = 700000;
                System.out.println("Fasilitas Kamar           : AC + Kamar Mandi Dalam");
                System.out.println("Harga Kamar               : " + harga);
                System.out.println("Pembayaran Setiap Tanggal : " + tgl);
                break;
            case 2:
                harga = 600000;
                System.out.println("Fasilitas Kamar           : AC + Kamar Mandi Luar");
                System.out.println("Harga Kamar               : " + harga);
                System.out.println("Pembayaran Setiap Tanggal : " + tgl);
                break;
            case 3:
                harga = 500000;
                System.out.println("Fasilitas Kamar           : Kipas + Kamar Mandi Dalam");
                System.out.println("Harga Kamar               : " + harga);
                System.out.println("Pembayaran Setiap Tanggal : " + tgl);
                break;
            case 4:
                harga = 400000;
                System.out.println("Fasilitas Kamar           : Kipas + Kamar Mandi Luar");
                System.out.println("Harga Kamar               : " + harga);
                System.out.println("Pembayaran Setiap Tanggal : " + tgl);
                break;
            default:
                System.out.println("Tidak Ada Dalam Pilihan");
        }
    }

    @Override
    public void output() {
        System.out.println("------------------ Bukti Sewa Kamar Kost ------------------");
        System.out.println("Nama Penyewa              : " + nama);
        System.out.println("Alamat Penyewa            : " + alamat);
        System.out.println("No HP Penyewa             : " + nohp);
        harga();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
