package awal;
interface daftar{
    public void daftar();
}

public class KamarKos implements daftar{
    @Override
    public void daftar() { // -	Daftar Objek-
        System.out.println("-------------- Daftar Jenis  Kamar dan Harga --------------");
        System.out.println("1. AC + Kamar Mandi Dalam      Rp700.000,-  perbulan");
        System.out.println("2. AC + Kamar Mandi Luar       Rp600.000,-  perbulan");
        System.out.println("3. Kipas + Kamar Mandi Dalam   Rp500.000,-  perbulan");
        System.out.println("4. Kipas + Kamar Mandi Luar    Rp400.000,-  perbulan");
    }
}