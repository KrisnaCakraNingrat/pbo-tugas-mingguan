package tugas_mingguan;
import java.util.Scanner;
public class tugas1 {
    public static void main(String[] args) {
        char nama[] = {"KRISNA", "CAKRA", "NINGRAT"} ; //tipe data char dan variabel, array 1 dimensi
	    String alamat[] = {"Kota Semarang","Provinsi Jawa Tengah"};//tipe data string dan variabel, array 1 dimensi
	    int umur, i, cari; //tipe data int dan variabel
	    char kata[] = "KRISNA";
	    int j = 0;
        Scanner keyboard = new Scanner(System.in);

        System.out.print ("\t\tTUGAS MINGGUAN \n") ;
	    System.out.print ("\nNama = ") ;
	
	    for (int i = 0; i < 18; i++) { //for
		    System.out.println (nama[i]) ;
	    }
	
	    System.out.print ("----------------------\n");
	    System.out.print ("\nAlamat = " );
	    for (int i = 0; i < 2; i++) {
		    System.out.println (alamat[i] + " ");
	    }
	
	    System.out.print ("----------------------\n");
        System.out.print ("\nTebak umur Krisna : "); //output
        umur = keyboard.nextInt(); //input
        if (umur == 19){ //if
            System.out.println ("WAHHHH KAMU BENARRR\n");
        }
        else {
            System.out.println ("MAAF COBA LAGI\n");
        }
    
        System.out.print ("----------------------\n\n");
	    while (kata[j] != kata[14]){ //while
		    System.out.println (kata[j]);
		    j++;
	    }
	
	    System.out.print ("----------------------\n\n");
    	int nilai=1;
    	do{ //do while
	    	System.out.println ("Berhitung = " + nilai);
		    nilai++;
    	}while(nilai<6);
	    System.out.print("Bye...\n");
	
	    System.out.print ("----------------------\n\n");
	    int arr[2][2]; //array multidimensi atau array 2 dimensi
 
	    arr[0][0] = 1;
	    arr[0][1] = 2;
	    arr[1][0] = 3;
	    arr[1][1] = 4;
	
	    System.out.println ("Isi variabel arr:" + "\n");
	    System.out.println (arr[0][0] +" "+ arr[0][1] + "\n");
	    System.out.println (arr[1][0] +" "+ arr[1][1] + "\n");        
    }
}