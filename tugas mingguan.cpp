#include <iostream>
using namespace std ;

int main() { //main program
	char nama[17] = {'K','R','I','S','N','A','_','C','A','K','R','A','_','N'} ; //tipe data char dan variabel, array 1 dimensi
	string alamat[2] = {"Kota Semarang","Provinsi Jawa Tengah"};//tipe data string dan variabel, array 1 dimensi
	int umur, i, cari; //tipe data int dan variabel
	char kata[14] = "KRISNA";
	int j = 0;
	
	cout << "\t\tTUGAS MINGGUAN \n" ;
	cout << "\nNama = " ;
	
	for (int i = 0; i < 18; i++) { //for
		cout << nama[i] ;
	}
	
	cout <<endl << "----------------------\n";
	cout << "\nAlamat = " ;
	for (int i = 0; i < 2; i++) {
		cout << alamat[i] << " ";
	}
	
	cout <<endl << "----------------------\n";
    cout << "\nTebak umur Krisna : "; //output
    cin >> umur; //input
    if (umur == 19){ //if
        cout << "WAHHHH KAMU BENARRR\n";
    }
    else {
        cout << "MAAF COBA LAGI\n";
    }
    
    cout <<endl << "----------------------\n\n";
	while (kata[j] != kata[14]){ //while
		cout<<kata[j]<<endl;
		j++;
	}
	
	cout <<endl << "----------------------\n\n";
	int nilai=1;
	do{ //do while
		cout<<"Berhitung = "<<nilai<<endl;
		nilai++;
	}while(nilai<6);
	cout<<"Bye...\n";
	
	cout <<endl << "----------------------\n\n";
	int arr[2][2]; //array multidimensi atau array 2 dimensi
 
	arr[0][0] = 1;
	arr[0][1] = 2;
	arr[1][0] = 3;
	arr[1][1] = 4;
	
	cout << "Isi variabel arr:" << endl;
	cout << arr[0][0] <<" "<< arr[0][1] << endl;
	cout << arr[1][0] <<" "<< arr[1][1] << endl;
	return 0;
}
