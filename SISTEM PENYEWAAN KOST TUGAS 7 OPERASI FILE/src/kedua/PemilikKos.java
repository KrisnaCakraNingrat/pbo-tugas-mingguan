package kedua;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class PemilikKos extends AwalData{
    private String username;
    private String nama;
    private String alamat;
    private int nohp;
    private int harga;
    private int tgl;
    private int pil;
    Scanner input = new Scanner(System.in);

    public PemilikKos() {
    }

    @Override
    public void data() {
        System.out.println("----------------- Masukkan Data Diri Anda -----------------");//Behavior
        System.out.print("Masukkan Nama Anda            : ");//Behavior
        nama = input.nextLine();
        System.out.print("Masukkan Alamat Anda          : ");//Behavior
        alamat = input.nextLine();
        System.out.print("Masukkan No HP Anda           : ");//Behavior
        nohp = input.nextInt();
        System.out.print("Masukkan Tanggal Masuk        : ");//Behavior
        tgl = input.nextInt();
        System.out.print("Masukkan Pilihan Kamar Anda   : ");//Behavior
        pil = input.nextInt();
    }

    @Override
    public void harga() {
        try {
            FileWriter fileWriter = new FileWriter("BuktiSewa.txt");

            fileWriter.write("------------------ Bukti Sewa Kamar Kost ------------------\n");
            fileWriter.write("Nama Penyewa              : " + nama + "\n");
            fileWriter.write("Alamat Penyewa            : " + alamat + "\n");
            fileWriter.write("No HP Penyewa             : " + nohp + "\n");

            switch (pil){
                case 1:
                    harga = 700000;
                    fileWriter.write("Fasilitas Kamar           : AC + Kamar Mandi Dalam\n");
                    fileWriter.write("Harga Kamar               : " + harga + "\n");
                    fileWriter.write("Pembayaran Setiap Tanggal : " + tgl + "\n");
                    fileWriter.close();
                    break;
                case 2:
                    harga = 600000;
                    fileWriter.write("Fasilitas Kamar           : AC + Kamar Mandi Luar\n");
                    fileWriter.write("Harga Kamar               : " + harga + "\n");
                    fileWriter.write("Pembayaran Setiap Tanggal : " + tgl + "\n");
                    fileWriter.close();
                    break;
                case 3:
                    harga = 500000;
                    fileWriter.write("Fasilitas Kamar           : Kipas + Kamar Mandi Dalam\n");
                    fileWriter.write("Harga Kamar               : " + harga + "\n");
                    fileWriter.write("Pembayaran Setiap Tanggal : " + tgl + "\n");
                    fileWriter.close();
                    break;
                case 4:
                    harga = 400000;
                    fileWriter.write("Fasilitas Kamar           : Kipas + Kamar Mandi Luar\n");
                    fileWriter.write("Harga Kamar               : " + harga + "\n");
                    fileWriter.write("Pembayaran Setiap Tanggal : " + tgl + "\n");
                    fileWriter.close();
                    break;
                default:
                    System.out.println("Tidak Ada Dalam Pilihan");
            }
        } catch (IOException e) {
            System.out.println("Pembuatan txt error...");
            throw new RuntimeException(e);
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}